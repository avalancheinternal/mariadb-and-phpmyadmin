# SETUP 
You can change this paramemeters in `docker-compose.yml` if you want, or live them as they are

MYSQL_ROOT_PASSWORD: password

MYSQL_DATABASE: databse

MYSQL_USER: user

MYSQL_PASSWORD: password


# COMMANDS
In bash `docker-compose up -d` to start

`docker-compose stop` to stop conteiners

`docker-compose down` to destroy containers

`docker-compose ps` list of running containers

## Importing database
docker exec -i docker-compose-mariadb_mariadb_1 mysql -uroot -ppassword YOUR_DATABASE_NAME < database.sql

# LINKS
[phpmyadmin - http://0.0.0.0:8087](http://0.0.0.0:8087)

[MariaDB - http://0.0.0.0:3307](http://0.0.0.0:3307)
